package com.celloscope.springbootstarter.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

@Controller
public class HelloController {

	@GetMapping("/")
	public String hello() {
		return "Hello from spring";
	}

	@GetMapping("/{name}")
	public String helloWithParameter(@PathVariable("name") String name) {
		return "Hello Celloscope from " + name;
	}
}
